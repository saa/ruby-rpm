#!/usr/bin/expect -f

spawn rpm --addsign {*}[lrange $argv 1 end]
expect -exact "Enter pass phrase: "
send -- "[lrange $argv 0 0]\n"
expect eof
