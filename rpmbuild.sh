#!/bin/bash
set -euxo pipefail

VERSION=$1
VERSION_NO_DOT=$(echo $VERSION | tr --delete .)
RELEASE=$2
SRC_SPEC=ruby-${VERSION}.spec
SPEC=./rpmbuild/SPECS/ruby-${VERSION}.spec

RHEL_VERSION=$(rpm -E %{rhel})
if [[ "$RHEL_VERSION" == "7" ]]; then
    DISTRIBUTION_SUFFIX="uvmsilk"
else
    DISTRIBUTION_SUFFIX="uvm"
    PYTHON3=/usr/libexec/platform-python
fi

mkdir -p ./rpmbuild/SOURCES ./rpmbuild/SPECS
cp $SRC_SPEC $SPEC
sed -i "s/\(\(Name\|Obsoletes\|Provides\): ruby\(gem\(s\)\?\)\?\)\(-\|(\)\?/\1${VERSION_NO_DOT}\5/" $SPEC
sed -i "s/^\(Source0: https:\/\/cache.ruby-lang.org\/pub\/ruby\/\)\(ruby-%{version}.tar.gz\)/\1${VERSION}\/\2/" $SPEC
sed -i "s/^\(export CFLAGS=\".*\)\(\"\)/\1 -Wl,-rpath,\/opt\/rubies\/ruby-${VERSION}\/lib64\2/" $SPEC
sed -i "s/^Release: [0-9]*/Release: ${RELEASE}/" $SPEC
sed -i "/^%prep/a %define _prefix /opt/rubies/ruby-${VERSION}" $SPEC
sed -i '/^%configure/a \ \ --prefix=%{_prefix} \\' $SPEC

cat <<HERE >~/.rpmmacros
%silk 1
%uvm_distribution_suffix $DISTRIBUTION_SUFFIX
%dist .el%{rhel}.%{uvm_distribution_suffix}
HERE

yum -y install rpm-build rpmdevtools yum-utils
if [[ "$RHEL_VERSION" == "8" ]]; then
    yum-config-manager --set-enabled powertools
fi

yum-builddep -y $SPEC
spectool -g -R -C ./rpmbuild/SOURCES $SPEC
rpmbuild -D "_topdir `pwd`/rpmbuild" -bb $SPEC
